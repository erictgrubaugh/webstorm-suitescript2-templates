/**
 * getInputDataDescription
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @returns {*} Data that will be used as input for the subsequent map or reduce
 *
 * @see [Help:getInputData]{https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4412447940.html}
 */
function getInputData(context) {
  log.audit({title: "Start ..."});

  // TODO
  #if ($parameterized == 'y')
  let params = _readParameters(runtime.getCurrentScript());

  return {
    type: "search|file",
    id: params.$paramKey,
    path: ""
  };
  #end
}
