    /**
     * <code>beforeSubmit</code> event handle
     * 
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.newRecord
	 * 		{record} The new record being submitted
     * @param context.oldRecord
	 * 		{record} The old record before it was modified
     * @param context.type
	 * 		{UserEventType} The action type that triggered this event
     * 
     * @return {void}
     * 
     * @static
     * @function beforeSubmit
     */
    function beforeSubmit(context) {
        // TODO
    }
