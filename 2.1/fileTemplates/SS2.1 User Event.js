#set($scriptType="UserEventScript")
#set($exports=[])
#parse("ss21.header.js")
#if ($beforeLoad == 'y')
#set($dummy=$exports.add("beforeLoad"))
#parse("ss21.ue.beforeLoad.js")
#end
#if ($beforeSubmit == 'y')
#set($dummy=$exports.add("beforeSubmit"))
#parse("ss21.ue.beforeSubmit.js")
#end
#if ($afterSubmit == 'y')
#set($dummy=$exports.add("afterSubmit"))
#parse("ss21.ue.afterSubmit.js")
#end
#parse("ss21.footer.js")
