/**
 * validateFieldDescription
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @returns {boolean} true if the field is valid; false to prevent the field value from changing.
 *
 * @see [Help:validateField]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4410693152.html}
 */
function validateField(context) {
  // TODO
  return true;
}
