#set($scriptType="MapReduceScript")
#set($exports=[])
#parse("ss21.header.js")
#if ($getInputData == 'y')
#set($dummy=$exports.add("getInputData"))
#parse("ss21.mr.getInputData.js")
#end
#if ($map == 'y')
#set($dummy=$exports.add("map"))
#parse("ss21.mr.map.js")
#end
#if ($reduce == 'y')
#set($dummy=$exports.add("reduce"))
#parse("ss21.mr.reduce.js")
#end
#if ($summarize == 'y')
#set($dummy=$exports.add("summarize"))
#parse("ss21.mr.summarize.js")
#end
#parse("ss21.footer.js")
