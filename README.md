## Overview ##

This project contains opinionated file templates that can be used by
[NetSuite](http://www.netsuite.com/portal/platform/developer.shtml)
developers who use the [WebStorm IDE](https://www.jetbrains.com/webstorm/) to
generate SuiteScript 2.0 and 2.1 script templates.

## Install ##

1. [Download the zip](https://gitlab.com/erictgrubaugh/webstorm-suitescript2-templates/repository/archive.zip?ref=master) of the source
1. Extract the archive
1. Close all instances of WebStorm.
1. Open both the `2.0` and `2.1` folders of the extracted archive. 
1. Copy the `fileTemplates` folder from each to:
    * `~/.WebStorm[Version Number]/config/` for WebStorm earlier than 2020.1
    * `%AppData%/Roaming/JetBrains/WebStorm[Version Number]/` for WebStorm after and including 2020.1
1. Launch (or restart) WebStorm

## Usage ##

SuiteScript templates can be used just like any other
[WebStorm file template](https://www.jetbrains.com/help/webstorm/11.0/file-and-code-templates.html).
Just click _File_ > _New_ and select the "SS2 *" script type you'd like to insert.

For script types with multiple entry-point handlers, you will be prompted for
which functions you'd like to generate in the template. Simply put a `y` in the
box for each function you'd like to generate. You don't need to put anything in
for the functions you don't want.

## Recommended plugins ##

I recommend also using the [Global File Template Variables](https://plugins.jetbrains.com/plugin/8008?pr=webStorm)
plugin to define the `author`, `email`, and `organization` variables for all
templates.
