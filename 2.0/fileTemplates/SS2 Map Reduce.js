#parse("ss2.header.js")
     
     * @NScriptType MapReduceScript
     */
    var exports = {};

#if ($getInputData == 'y')
#parse("ss2.mr.getInputData.js")
#end

#if ($map == 'y')
#parse("ss2.mr.map.js")
#end

#if ($reduce == 'y')
#parse("ss2.mr.reduce.js")
#end

#if ($summarize == 'y')
#parse("ss2.mr.summarize.js")
#end

#if ($getInputData == 'y')
    #parse("ss2.mr.getInputData.export.js")
#end
#if ($map == 'y')
    #parse("ss2.mr.map.export.js")
#end
#if ($reduce == 'y')
    #parse("ss2.mr.reduce.export.js")
#end
#if ($summarize == 'y')
    #parse("ss2.mr.summarize.export.js")
#end
#parse("ss2.footer.js")
