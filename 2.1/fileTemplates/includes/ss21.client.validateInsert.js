/**
 * validateInsertDescription
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @returns {boolean} true if the line can be inserted; false to prevent the line insertion.
 *
 * @see [Help:validateInsert]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4410693455.html}
 */
function validateInsert(context) {
  // TODO
  return true;
}
