#set($scriptType="WorkflowActionScript")
#set($exports=[])
#parse("ss21.header.js")
#if ($beforeInstall == 'y')
#set($dummy=$exports.add("beforeInstall"))
#parse("ss21.bi.beforeInstall.js")
#end
#if ($afterInstall == 'y')
#set($dummy=$exports.add("afterInstall"))
#parse("ss21.bi.afterInstall.js")
#end
#if ($beforeUpdate == 'y')
#set($dummy=$exports.add("beforeUpdate"))
#parse("ss21.bi.beforeUpdate.js")
#end
#if ($afterUpdate == 'y')
#set($dummy=$exports.add("afterUpdate"))
#parse("ss21.bi.afterUpdate.js")
#end
#if ($beforeUninstall == 'y')
#set($dummy=$exports.add("beforeUninstall"))
#parse("ss21.bi.beforeUninstall.js")
#end
#parse("ss21.footer.js")
