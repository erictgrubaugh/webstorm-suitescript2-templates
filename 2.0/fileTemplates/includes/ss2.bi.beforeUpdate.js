    /**
     * <code>beforeUpdate</code> event handler
     *
     * @gov XXX
     *
     * @param params
     * 		{Object}
     * @param params.fromVersion
     * 		{number} The version of the bundle that is currently installed
     * @param params.toVersion
     * 		{number} The version of the bundle that is being installed
     *
     * @return {void}
     *
     * @static
     * @function beforeUpdate
     */
    function beforeUpdate(params) {
        // TODO
    }
