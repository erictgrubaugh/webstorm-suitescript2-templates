    /**
     * <code>validateLine</code> event handler
     *
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{string} The internal ID of the sublist.
     *
     * @return {boolean} <code>true</code> if the line is valid;
     *         <code>false</code> to prevent the line submission.
     *
     * @static
     * @function validateLine
     */
    function validateLine(context) {
    	// TODO
        return true;
    }
