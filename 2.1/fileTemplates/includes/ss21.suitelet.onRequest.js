/**
 * onRequestDescription
 *
 * @gov XXX
 *
 * @param {Object} context Contains references to the incoming request and outgoing response
 *
 * @see [Help:onRequest]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4407987288.html}
 */
function onRequest(context) {
  log.audit({title: `${context.request.method} request received`});

  const eventRouter = {
    #if ($get == "y")
    [https.Method.GET]: onGet,
    #end
    #if ($post == "y")
    [https.Method.POST]: onPost
    #end
  };

  try {
    (eventRouter[context.request.method])(context);
  } catch (e) {
    onError({context, error: e});
  }

  log.audit({title: "Request complete."});
}
