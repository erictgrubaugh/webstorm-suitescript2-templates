#set($scriptType="Restlet")
#set($exports=[])
#parse("ss21.header.js")
#if ($get == 'y')
#set($dummy=$exports.add("_get"))
#parse("ss21.restlet.get.js")
#end
#if ($post == 'y')
#set($dummy=$exports.add("post"))
#parse("ss21.restlet.post.js")
#end
#if ($put == 'y')
#set($dummy=$exports.add("put"))
#parse("ss21.restlet.put.js")
#end
#if ($delete == 'y')
#set($dummy=$exports.add("_delete"))
#parse("ss21.restlet.delete.js")
#end
#parse("ss21.footer.js")
