/**
 * getDescription
 *
 * @gov XXX
 *
 * @param {Object} params - The parameters from the HTTP request URL.
 *
 * @returns {string|Object} Returns a String when request Content-Type is text/plain;
 *  returns an Object when request Content-Type is application/json
 *
 * @see [Help:get]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4407965171.html}
 */
function _get(params) {
  // TODO
}
