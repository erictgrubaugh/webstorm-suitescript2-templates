/**
 * validateDeleteDescription
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @returns {boolean} true to allow the removal; false to prevent the removal.
 *
 * @see [Help:validateDelete]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4410693608.html}
 */
function validateDelete(context) {
  // TODO
  return true;
}
