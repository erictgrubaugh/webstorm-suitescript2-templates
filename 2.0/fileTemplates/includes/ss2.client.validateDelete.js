    /**
     * <code>validateDelete</code> event handler
     *
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.currentRecord
	 * 		{record} The current record the user is manipulating in the UI
     * @param context.sublistId
	 * 		{string} The internal ID of the sublist.
     *
     * @return {boolean} <code>true</code> if the line can be removed;
     *         <code>false</code> to prevent the line removal.
     *
     * @static
     * @function validateDelete
     */
    function validateDelete(context) {
    	// TODO
        return true;
    }
