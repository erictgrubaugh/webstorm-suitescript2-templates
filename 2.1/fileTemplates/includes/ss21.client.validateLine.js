/**
 * validateLineDescription
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @returns {boolean} true to allow line addition; false to prevent line addition.
 *
 * @see [Help:validateLine]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4410693302.html}
 */
function validateLine(context) {
  // TODO
  return true;
}
