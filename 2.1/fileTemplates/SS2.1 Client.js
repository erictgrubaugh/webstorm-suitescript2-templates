#set($scriptType="ClientScript")
#set($exports=[])
#parse("ss21.header.js")
#if ($pageInit == 'y')
#set($dummy=$exports.add("pageInit"))
#parse("ss21.client.pageInit.js")
#end
#if ($validateField == 'y')
#set($dummy=$exports.add("validateField"))
#parse("ss21.client.validateField.js")
#end
#if ($fieldChanged == 'y')
#set($dummy=$exports.add("fieldChanged"))
#parse("ss21.client.fieldChanged.js")
#end
#if ($localizationContextEnter == 'y')
#set($dummy=$exports.add("localizationContextEnter"))
#parse("ss21.client.localizationContextEnter.js")
#end
#if ($localizationContextExit == 'y')
#set($dummy=$exports.add("localizationContextExit"))
#parse("ss21.client.localizationContextExit.js")
#end
#if ($postSourcing == 'y')
#set($dummy=$exports.add("postSourcing"))
#parse("ss21.client.postSourcing.js")
#end
#if ($lineInit == 'y')
#set($dummy=$exports.add("lineInit"))
#parse("ss21.client.lineInit.js")
#end
#if ($validateLine == 'y')
#set($dummy=$exports.add("validateLine"))
#parse("ss21.client.validateLine.js")
#end
#if ($validateInsert == 'y')
#set($dummy=$exports.add("validateInsert"))
#parse("ss21.client.validateInsert.js")
#end
#if ($validateDelete == 'y')
#set($dummy=$exports.add("validateDelete"))
#parse("ss21.client.validateDelete.js")
#end
#if ($sublistChanged == 'y')
#set($dummy=$exports.add("sublistChanged"))
#parse("ss21.client.sublistChanged.js")
#end
#if ($saveRecord == 'y')
#set($dummy=$exports.add("saveRecord"))
#parse("ss21.client.saveRecord.js")
#end
#parse("ss21.footer.js")
