    /**
     * <code>afterUpdate</code> event handler
     *
     * @gov XXX
     *
     * @param params
     * 		{Object}
     * @param params.fromVersion
     * 		{number} The version of the bundle that was previously installed
     * @param params.toVersion
     * 		{number} The version of the bundle that was just installed
     *
     * @return {void}
     *
     * @static
     * @function afterUpdate
     */
    function afterUpdate(params) {
        // TODO
    }
