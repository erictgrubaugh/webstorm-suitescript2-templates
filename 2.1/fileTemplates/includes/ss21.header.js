/**
 * ${NAME}
 *
 * moduleDescription
 *
 * @module ${NAME}
 *
 * @copyright ${YEAR} ${organization}
 * @author ${author} <${email}>
 * @license MIT
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 * @NScriptType ${scriptType}
 */
define([

], (

) => {
