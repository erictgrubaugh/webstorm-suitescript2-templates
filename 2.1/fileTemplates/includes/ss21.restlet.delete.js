/**
 * deleteDescription
 *
 * @gov XXX
 *
 * @param {Object} params - The parameters from the HTTP request URL.
 *
 * @returns {string|Object} Returns a String when request Content-Type is text/plain;
 *   returns an Object when request Content-Type is application/json
 *
 * @see [Help:delete]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4407965553.html}
 */
function _delete(params) {
  // TODO
}
