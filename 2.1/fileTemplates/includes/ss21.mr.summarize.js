/**
 * summarizeDescription
 *
 * @gov XXX
 *
 * @param {Object} summary
 *
 * @see [Help:summarize]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4413276323.html}
 */
function summarize(summary) {
  let errors = _parseErrors(summary);

  // TODO

  log.audit({title: "Complete."});
}

/**
 * Parses errors from all stages into a single list
 *
 * @gov 0
 *
 * @param {SummaryContext} summary - Holds statistics regarding execution of the script
 *
 * @returns {Object[]} list of errors encountered while running the script
 */
function _parseErrors(summary) {
  let errors = [];

  if (summary.inputSummary.error) {
      errors.push(summary.inputSummary.error);
  }

  summary.mapSummary.errors.iterator().each((k, e) => errors.push(e));
  summary.reduceSummary.errors.iterator().each((k, e) => errors.push(e));

  return errors;
}

#if ($parameterized == 'y')
/**
 * Reads relevant script parameters from the given Script instance
 *
 * @gov 0
 *
 * @param {Script} script The script to read
 *
 * @returns {Object} Key-value pairs of relevant script parameters from the given script
 */
function _readParameters(script) {
  return {
    $paramKey: script.getParameter({name: '$paramKey'})
  };
}
#end
