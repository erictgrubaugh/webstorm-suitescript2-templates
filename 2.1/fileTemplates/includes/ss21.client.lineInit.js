/**
 * lineInit event handler; executed when an existing line is selected.
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @see [Help:lineInit]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4410693004.html}
 */
function lineInit(context) {
  // TODO
}
