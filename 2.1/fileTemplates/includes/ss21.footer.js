#set($export="placeholder")
#set($foreach="placeholder")
  return /** @alias module:${NAME} */ {
    #foreach($export in $exports)
    $export#if($foreach.hasNext),
    #end
    #end
  };
});
