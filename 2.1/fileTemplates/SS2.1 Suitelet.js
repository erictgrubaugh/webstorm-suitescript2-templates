#set($scriptType="Suitelet")
#set($exports=["onRequest"])
#parse("ss21.header.js")
#parse("ss21.suitelet.onRequest.js")
#if ($get == "y")
#parse("ss21.suitelet.get.js")
#end
#if ($post == "y")
#parse("ss21.suitelet.post.js")
#end
#parse("ss21.suitelet.error.js")
#parse("ss21.footer.js")
