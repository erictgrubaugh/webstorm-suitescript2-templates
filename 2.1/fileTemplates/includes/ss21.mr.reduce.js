/**
 * reduceDescription
 *
 * @gov XXX
 *
 * @param {Object} context
 *
 * @see [Help:reduce]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4413276172.html}
 */
function reduce(context) {
  log.audit({title: "[reduce] Processing Key:", details: context.key});

  let values = context.values.map(JSON.parse);

  // TODO
}
