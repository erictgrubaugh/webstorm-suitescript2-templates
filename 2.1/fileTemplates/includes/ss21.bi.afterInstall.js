/**
 * afterInstallDescription
 *
 * @gov XXX
 *
 * @param {Object} params Contains the version of the bundle that was just installed
 *
 * @see [Help:afterInstall]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4460460346.html}
 */
function afterInstall(params) {
  // TODO
}
