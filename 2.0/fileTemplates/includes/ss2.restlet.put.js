    /**
     * <code>put</code> event handler
     * 
     * @gov XXX
     * 
     * @param request
	 * 		{String|Object} The request body as a String when
     *            <code>Content-Type</code> is <code>text/plain</code>; The
     *            request body as an Object when request
     *            <code>Content-Type</code> is <code>application/json</code>
     * 
     * @return {String|Object} Returns a String when request
     *         <code>Content-Type</code> is <code>text/plain</code>;
     *         returns an Object when request <code>Content-Type</code> is
     *         <code>application/json</code>
     * 
     * @static
     * @function put
     */
    function put(request) {
        // TODO
    }
