    /**
     * <code>pageInit</code> event handler
     *
     * @gov XXX
     *
     * @param context
	 * 		{Object}
     * @param context.mode
	 * 		{string} The access mode of the current record. Will be one of
     *            <ul>
     *            <li>copy</li>
     *            <li>create</li>
     *            <li>edit</li>
     *            </ul>
     * @param context.currentRecord {CurrentRecord} The record in context
     *
     * @return {void}
     *
     * @static
     * @function pageInit
     */
    function pageInit(context) {
    	// TODO
    }
