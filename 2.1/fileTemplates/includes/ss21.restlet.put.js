/**
 * putDescription
 *
 * @gov XXX
 *
 * @param {string|Object} request - The request body as a String when Content-Type is
 *  text/plain; The request body as an Object when request Content-Type is application/json
 *
 * @returns {string|Object} Returns a String when request Content-Type is text/plain;
 *  returns an Object when request Content-Type is application/json
 *
 * @see [Help:put]{@link https://system.netsuite.com/app/help/helpcenter.nl?fid=section_4407965751.html}
 */
function put(request) {
  // TODO
}
